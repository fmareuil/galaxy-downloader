# Galaxy Downloader


### Installation

* install [python >= 3.8](https://www.python.org/)
* install [pip](https://pip.pypa.io/en/stable/installation/)
* install [bioblend](https://bioblend.readthedocs.io/en/latest/)
``pip install bioblend``


### Usage

* To obtain some help:
``python galaxy_downloader.py -h``

* Default usage (warning this exemple download all the data of your history:
``python galaxy_downloader.py -g galaxy_url -k api_key -gh history_name -gd '\w*'``
