"""
Created on Fev. 02, 2022

@authors: Fabien Mareuil, Institut Pasteur, Paris
@contacts: fabien.mareuil@pasteur.fr
@project: galaxy_downloader
@githuborganization: Hub
"""

"""
This script aims to download dataset from
a given Galaxy instance.
"""

import sys
import os
import re
import logging
import argparse
from bioblend import galaxy

_LOGGER = logging.getLogger()


def logger_level(args):
    """
    Define level of logger.
    """
    if args.verbose:
        return logging.INFO
    return logging.WARNING
    
def parse_arguments(args):
    """
    Define parser for the script
    """
    parser = argparse.ArgumentParser(description='download dataset from Galaxy server')
    parser.add_argument('-g', '--galaxy', help='Galaxy server URL', required=True)
    parser.add_argument('-k', '--api_key', help='API key from Galaxy (see user preferences/Manage API key on your galaxy server)', required=True)
    parser.add_argument('-gh', '--history', help='history name (be sure it is unique)', required=True)
    parser.add_argument('-gd', '--dataset_pattern', help='dataset name pattern in the given galaxy history (use a regular expression)', required=True)
    parser.add_argument('-o', '--output_directory', help='directory output (directory where data will be saved)')
    parser.add_argument('-v', '--verbose', help='verbose mode', action='store_true')

    try:
        return parser.parse_args(args)
    except SystemExit:
        sys.exit(1)
	
def run():
    # Parse arguments
    args = parse_arguments(sys.argv[1:])

    # Configure logger
    logging.basicConfig(level=logger_level(args))
    if args.output_directory == None:
        output_directory = os.getcwd()
    else:
        output_directory = args.output_directory

    # Connect to Galaxy instance
    gi = galaxy.GalaxyInstance(url=args.galaxy, key=args.api_key)

    histories = gi.histories.get_histories(name=args.history)    
    if len(histories) > 1:
        _LOGGER.warning('Several histories are named {}.'.format(args.history))
        sys.exit(1)
    elif len(histories) == 1:
        _LOGGER.warning('One history named {} has been found.'.format(args.history))
    else:
       _LOGGER.warning('Any history named {} has been found.'.format(args.history))
       sys.exit(1) 
    history = gi.histories.show_history(histories[0]['id'], contents=True)
    
    count = 0
    for data in history:
        if re.match(args.dataset_pattern, data['name']):
           count += 1
           _LOGGER.warning('Dataset {} named "{}" is downloading in {}'.format(data['id'], data['name'], output_directory))
           gi.datasets.download_dataset(data['id'], file_path=output_directory)
           _LOGGER.warning('Finished')
    _LOGGER.warning('{} dataset have been downloaded.'.format(count))
	
if __name__ == '__main__':
    run()
